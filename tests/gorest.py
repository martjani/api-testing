from utils.gorest_handler import GoRESTHandler
from faker import Faker

gorest_handler = GoRESTHandler()

user_data = {
    "name": "Vid Gowda",
    "email": Faker().email(),
    "gender": "female",
    "status": "active",
}

user_data_1 = {
    "name": "Kate Murphy",
    "email": Faker().email(),
    "gender": "female",
    "status": "active",
}


def test_CRUD_user():
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]

    body = gorest_handler.update_user(user_id, user_data_1).json()
    assert body["name"] != user_data["name"]
    assert body["email"] != user_data["email"]

    body = gorest_handler.delete_user(user_id)
